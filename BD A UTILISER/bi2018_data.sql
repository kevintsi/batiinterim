USE `bi2018`;		

--
-- Table data for table `Secteur`
--

INSERT INTO `Secteur` VALUES 
	(1, 'Structure et gros oeuvre'),
	(2, 'Enveloppe extérieure'),
	(3, 'Equipement technique'),
	(4, 'Aménagements et finitions');

--
-- Table data for table `CorpsMetier`
--

INSERT INTO `CorpsMetier` VALUES 
	(1, 'Charpentier bois', 1),
	(2, 'Constructeur bois', 1),
	(3, 'Constructeur en béton',1),
	(4, 'Constructeur en béton armé',1),
	(5, 'Couvreur',2),
	(6, 'Etancheur',2),
	(7, 'Miroitier', 2),
	(8, 'Chauffagiste', 3),
	(9, 'Electricien', 3),
	(10, 'Agenceur', 4),
	(11, 'Carreleur', 4),
	(12, 'Menuisier', 4),
	(13, 'Peintre', 4),
	(14, 'Electricien', 4);

--
-- Table data for table `Artisan`
--

INSERT INTO `Artisan` VALUES 
	(1, 'Dupont', 'Paul', '1960-04-12','Poitiers','France','0678945321','2 rue des moulins','91830', 'Auvernaux','','',false,11),
	(2, 'Durand','Albert','1970-02-22','Paris','France','0667548393','14, avenue des roses','91290', 'Arpajon','','',false, 12);
--
-- Table structure for table `Conge`
--

INSERT INTO `Conge` VALUES
	(1, '2018-09-01','2018-09-01','Validé',1),
	(2, '2018-12-22','2018-12-28','En attente de validation',1);

--
-- Table data for table `Entrepreneur`
--

INSERT INTO `Entrepreneur` VALUES 
	(1, 'Square Rénovation','Garcia', 'Georges','0789098745','0148421870','3 rue du Dr Jaquemaire Clémenceau', '75015','Paris','','',false);

--
-- Table  data for table `ChefChantier`
--

INSERT INTO `ChefChantier` VALUES 
	(1, 1, 'Tarnot','Eric');
