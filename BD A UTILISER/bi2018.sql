-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: bi2018
-- ------------------------------------------------------
-- Server version	5.5.41-0+wheezy1

DROP DATABASE IF EXISTS `bi2018`;  
CREATE DATABASE `bi2018`;			

USE `bi2018`;		

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Secteur`
--

DROP TABLE IF EXISTS `Secteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Secteur` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `libelleSecteur` varchar(128) DEFAULT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CorpsMetier`
--

DROP TABLE IF EXISTS `CorpsMetier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CorpsMetier` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `libelleCorpsMetier` varchar(128) DEFAULT NULL,
	  `idSecteur` integer NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `Artisan`
--

DROP TABLE IF EXISTS `Artisan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Artisan` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `nomArtisan` varchar(128) DEFAULT NULL,
	  `prenomArtisan` varchar(128) DEFAULT NULL,
	  `dateNaissanceArtisan` date DEFAULT NULL,
	  `villeNaissanceArtisan` varchar(128) DEFAULT NULL,
	  `paysNaissanceArtisan` varchar(128) DEFAULT NULL,
	  `telephoneArtisan` char(10) DEFAULT NULL,
	  `adresseArtisan` varchar(128) DEFAULT NULL,
	  `cpArtisan` char(5) DEFAULT NULL,
	  `villeArtisan` varchar(128) DEFAULT NULL,
	  `loginArtisan` varchar(10) DEFAULT NULL,
	  `mdpArtisan` varchar(512) DEFAULT NULL, 
	  `premiereConnexion` boolean NOT NULL,
	  `idCorpsMetier` integer NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Conge`
--

DROP TABLE IF EXISTS `Conge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Conge` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `dateDebutConge` date DEFAULT NULL,
	  `dateFinConge` date DEFAULT NULL,
	  `etatConge` varchar(128) DEFAULT NULL,
	  `idArtisan` integer NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Entrepreneur`
--

DROP TABLE IF EXISTS `Entrepreneur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Entrepreneur` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `raisonSociale` varchar(128) DEFAULT NULL,
	  `nomResponsable` varchar(128) DEFAULT NULL,
	  `prenomResponsable` varchar(128) DEFAULT NULL,
	  `telResponsable` varchar(128) DEFAULT NULL,
	  `telephoneEntreprise` char(10) DEFAULT NULL,
	  `adresseEntreprise` varchar(128) DEFAULT NULL,
	  `cpEntreprise` char(5) DEFAULT NULL,
	  `villeEntreprise` varchar(128) DEFAULT NULL,
	  `loginEntreprise` varchar(10) DEFAULT NULL,
	  `mdpEntreprise` varchar(512) DEFAULT NULL,
	  `premiereConnexion` boolean NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `ChefChantier`
--

DROP TABLE IF EXISTS `ChefChantier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChefChantier` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `idEntrepreneur` integer NOT NULL,
	  `nomChefChantier` varchar(128) DEFAULT NULL,
	  `prenomChefChantier` varchar(128) DEFAULT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;





--
-- Table structure for table `Mission`
--

DROP TABLE IF EXISTS `Mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mission` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `intituleMission` varchar(128) DEFAULT NULL,
	  `nombreArtisans` integer DEFAULT NULL,
	  `prixJournalier` integer DEFAULT NULL,
	  `dateDebutMission` date DEFAULT NULL,
	  `dateFinMission` date DEFAULT NULL,
	  `idChantier` integer NOT NULL,
	  `idCorpsMetier` integer NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `affecter`
--

DROP TABLE IF EXISTS `Affecter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Affecter` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `idMission` integer NOT NULL,
	  `idArtisan` integer NOT NULL,
	  `dateAffectation` date DEFAULT NULL,
	  `etatAffectation` char DEFAULT NULL,
	  `dateValidation` date DEFAULT NULL,
	   PRIMARY KEY(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Appartenir`
--
DROP TABLE IF EXISTS `Travailler`;
CREATE TABLE `Travailler` (
	`id` integer NOT NULL AUTO_INCREMENT,
	`idSecteur` integer NOT NULL,
	`idEntrepreneur` integer NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
		

--
-- Table structure for table `Chantier`
--

DROP TABLE IF EXISTS `Chantier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Chantier` (
	  `id` integer NOT NULL AUTO_INCREMENT,
	  `libelleChantier` varchar(128) NULL, 
	  `dateDebutChantier` date  NOT NULL,
	  `dateFinChantier` date NOT NULL,
	  `adresseChantier` varchar(128) NULL,
	  `cpChantier` char(5) NULL,
	  `villeChantier` varchar(128) NULL,
	  `idChefChantier` integer NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

ALTER TABLE Artisan ADD CONSTRAINT `FK_ARTISAN_METIER` 
	FOREIGN KEY (`idCorpsMetier`) REFERENCES `CorpsMetier` (`id`);
		
ALTER TABLE Conge ADD CONSTRAINT `FK_Conge_Artisan` 
	FOREIGN KEY(`idArtisan`) REFERENCES Artisan(`id`);
			
ALTER TABLE ChefChantier ADD CONSTRAINT `FK_ChefChantier_Entrepreneur` 
	FOREIGN KEY (`idEntrepreneur`) REFERENCES Entrepreneur(`id`);

ALTER TABLE Chantier ADD CONSTRAINT `FK_Chantier_ChefChantier` 
	FOREIGN KEY(`idChefChantier`) REFERENCES ChefChantier(`id`);

ALTER TABLE Mission ADD CONSTRAINT `FK_Mission_Chantier`
	FOREIGN KEY(`idChantier`) REFERENCES `Chantier`(`id`);

ALTER TABLE Mission ADD CONSTRAINT `FK_Mission_CorpsMetier` 
	FOREIGN KEY(`idCorpsMetier`) REFERENCES CorpsMetier(`id`);

ALTER TABLE Affecter ADD CONSTRAINT `FK_Affecter_Artisan` 
	FOREIGN KEY (`idArtisan`) REFERENCES `Artisan` (`id`);

ALTER TABLE Affecter ADD CONSTRAINT `FK_Affecter_Mission` 
	FOREIGN KEY (`idMission`) REFERENCES `Mission` (`id`);

ALTER TABLE CorpsMetier ADD CONSTRAINT `FK_CorpsMetier_Secteur`
	FOREIGN KEY (`idSecteur`) REFERENCES `Secteur`(`id`);

ALTER TABLE Travailler ADD CONSTRAINT `FK_Travailler_Secteur`
	FOREIGN KEY(`idSecteur`) REFERENCES `Secteur`(`id`);

ALTER TABLE Travailler ADD CONSTRAINT `FK_Travailler_Entrepreneur`
	FOREIGN KEY(`idEntrepreneur`) REFERENCES `Entrepreneur`(`id`);

