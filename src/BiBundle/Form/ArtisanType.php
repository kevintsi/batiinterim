<?php

namespace BiBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtisanType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomartisan')
                ->add('prenomartisan')
                ->add('datenaissanceartisan', BirthdayType::class)
                ->add('villenaissanceartisan')
                ->add('paysnaissanceartisan', CountryType::class)
                ->add('telephoneartisan')
                ->add('adresseartisan')
                ->add('cpartisan')
                ->add('villeartisan')
                ->add('idcorpsmetier', EntityType::class, array('class'=>'BiBundle:Corpsmetier',
                                                                'choice_label'=>'libellecorpsmetier',
                                                                'multiple'=>false))
                ->add('valider', SubmitType::class, array('attr' => array('class'=>'btn btn-primary')));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BiBundle\Entity\Artisan'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bibundle_artisan';
    }


}
