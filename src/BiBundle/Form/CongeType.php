<?php

namespace BiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CongeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('datedebutconge', DateType::class, array('label'=> 'Date de début de congé : '))
                ->add('datefinconge', DateType::class, array('label'=> 'Date de fin de congé : '))
                /*->add('etatconge', ChoiceType::class, array(
                                                            'choices'=> array(
                                                                              'Accepter'=>'A', 'Refuser'=>'R')))*/
                ->add('valider', SubmitType::class, array('attr'=>array('class'=>'btn btn-primary')));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BiBundle\Entity\Conge'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bibundle_conge';
    }


}
