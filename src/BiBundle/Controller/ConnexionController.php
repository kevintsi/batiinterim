<?php

namespace BiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Session\Session;

class ConnexionController extends Controller
{
    public function connexionAction(Request $request)
    {
        $form = $this->createFormBuilder()
                     ->add('login', TextType::class)
                     ->add('mdp', PasswordType::class)
                     ->add('valider', SubmitType::class)
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
          if($form->get('login')->getData() == 'admin' && $form->get('mdp')->getData() == 'azerty')
          {
            return $this->redirectToRoute('bi_homepage', array('profil'=>'gestionnaire'));
          }
          else
          {
            $em = $this->getDoctrine()->getManager();
            $unArtisan = $em->getRepository('BiBundle:Artisan')->findByLoginAndMdp($form->get('login')->getData(), $form->get('mdp')->getData());
            $unEntrepreneur = $em->getRepository('BiBundle:Entrepreneur')->findByLoginAndMdp($form->get('login')->getData(), $form->get('mdp')->getData());
            if($unArtisan != null)
            {
              $session = new Session();
              $session->set('id', $unArtisan->getId());

              return $this->redirectToRoute('bi_homepage', array('profil'=>'artisan'));
            }
            else if($unEntrepreneur != null)
            {
              $session = new Session();
              $session->set('id', $unEntrepreneur->getId());

              return $this->redirectToRoute('bi_homepage', array('profil'=>'entrepreneur'));
            }
          }
        }
      return $this->render('@Bi/Connexion/formulaireConnexion.html.twig', array('form'=>$form->createView()));
    }

    public function deconnexionAction()
    {
      $session = new Session();
      $session->clear();
      $session->invalidate();

      return $this->redirectToRoute('bi_connexion');
    }
    public function premiereConnexionAction(Request $request)
    {
        $form = $this->createFormBuilder()
                   ->add('login', TextType::class)
                   ->add('valider', SubmitType::class)
                   ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
          $data = $form->getData();
          $login = $data['login'];
          $em = $this->getDoctrine()->getManager();
          $leArtisan = $em->getRepository('BiBundle:Artisan')->findOneByloginartisan($login);
          dump($leArtisan);
          $leEntrepreneur = $em->getRepository('BiBundle:Entrepreneur')->findOneByloginentreprise($login);
          dump($leEntrepreneur);

          if($leArtisan != null)
          {
            if($leArtisan->getPremiereconnexion() == 1)
            {
              $id = $leArtisan->getId();
              return $this->redirectToRoute('bi_mdpPremiere', array('id'=>$id));
            }
            else
            {
              return $this->redirectToRoute('bi_connexion');
            }
          }
          if($leEntrepreneur != null)
           {
             if($leEntrepreneur->getPremiereconnexion() == 1)
             {
               $id = $leEntrepreneur->getId();
               return $this->redirectToRoute('bi_mdpPremiere', array('id'=>$id));
             }
             else
             {
               return $this->redirectToRoute('bi_connexion');
             }
           }
          }
          return $this->render('@Bi/Connexion/premiereConnexion.html.twig', array('form'=>$form->createView()));
        }

    public function changeMdpAction($id, Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $unArtisan = $em->getRepository('BiBundle:Artisan')->findOneByid($id);
      $unEntrepreneur = $em->getRepository('BiBundle:Entrepreneur')->findOneByid($id);
      dump($unEntrepreneur);

      if($unArtisan != null)
      {
        $form = $this->createFormBuilder()
                     ->add('mdp', PasswordType::class)
                     ->add('valider', SubmitType::class)
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
          $unArtisan->setMdpartisan(sha1($form->get('mdp')->getData()))
                    ->setPremiereconnexion(0);
          $em->persist($unArtisan);
          $em->flush();

          return $this->redirectToRoute('bi_connexion');
        }
        return $this->render('@Bi/Connexion/formulaireMdp.html.twig', array('form'=>$form->createView()));
      }
      if($unEntrepreneur != null)
      {
        $form = $this->createFormBuilder()
                     ->add('mdp', PasswordType::class)
                     ->add('valider', SubmitType::class)
                     ->getForm();
        dump($form);
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
          // dump($unEntrepreneur);
          $unEntrepreneur->setMdpentreprise(sha1($form->get('mdp')->getData()))
                         ->setPremiereconnexion(0);
          $em->persist($unEntrepreneur);
          $em->flush();

          return $this->redirectToRoute('bi_connexion');
        }
        return $this->render('@Bi/Connexion/formulaireMdp.html.twig', array('form'=>$form->createView()));
      }
    }
}
