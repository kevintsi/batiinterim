<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BiBundle\Controller;
use BiBundle\Entity\Conge;

use BiBundle\Form\CongeType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BiBundle\Form\ArtisanType;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Description of ArtisanController
 *
 * @author kevintsi
 */
class ArtisanController extends Controller {

    public function accueilAction(){

        return $this->render("@Bi/Artisan/accueilArtisan.html.twig");
    }
    /*public function getLesCorpsMetierAction(){

        //$db = $this->get('tkbat.PdoConstructeur'); // new BatInterim(dsn, user, password);
        $db = $this->get('tkbat.monPdo');
        $service = new BatInterim($db);
        $LesCorpsMetier = $service->getLesCorpsMetier();

        return $this->render("@TKBat/Artisan/listeCorpsMetier.html.twig",array('LesCorpsMetier'=> $LesCorpsMetier));
    }*/
    public function creerJourCongeAction(Request $request){
        $conge = new Conge();
        $form = $this->createForm(CongeType::class, $conge);

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
          $em = $this->getDoctrine()->getManager();
          $session = new Session();
          $id = $session->get('id');
          $artisan = $em->getRepository('BiBundle:Artisan')->find($id);
          $conge->setDatedebutconge($form->get('datedebutconge')->getData())
                ->setDatefinconge($form->get('datefinconge')->getData())
                ->setEtatconge('En attente de validation')
                ->setIdartisan($artisan);
          $em->persist($conge);
          //dump($conge);
          $em->flush();

          return $this->render("@Bi/Artisan/creeCongeArtisan.html.twig", array('formConge'=> $form->createView()));

        }
        return $this->render("@Bi/Artisan/creeCongeArtisan.html.twig", array('formConge'=> $form->createView()));
    }
    public function consulterCongeAction()
    {
      $em = $this->getDoctrine()->getManager();
      $session = new Session();
      $id = $session->get('id');
      $lesConges = $em->getRepository('BiBundle:Conge')->findByidartisan($id);
      //dump($lesConges);

      return $this->render('@Bi/Artisan/voirConge.html.twig', array('lesConges'=>$lesConges));
    }
    public function supprimerCongeAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $leConge = $em->getRepository('BiBundle:Conge')->find($id);
      //dump($leConge);
      $em->remove($leConge);
      $em->flush();

      return $this->redirectToRoute('bi_consulterCongeArtisan');
    }
    public function modifierArtisanAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $session = new Session();
      $id = $session->get('id');
      $unArtisan = $em->getRepository('BiBundle:Artisan')->find($id);

      $form = $this->createForm(ArtisanType::class, $unArtisan);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid())
      {
        $unArtisan->setNomartisan($form->get('nomartisan')->getData())
                  ->setPrenomartisan($form->get('prenomartisan')->getData())
                  ->setDatenaissanceartisan($form->get('datenaissanceartisan')->getData())
                  ->setVillenaissanceartisan($form->get('villenaissanceartisan')->getData())
                  ->setPaysnaissanceartisan($form->get('paysnaissanceartisan')->getData())
                  ->setTelephoneartisan($form->get('telephoneartisan')->getData())
                  ->setAdresseartisan($form->get('adresseartisan')->getData())
                  ->setCpartisan($form->get('cpartisan')->getData())
                  ->setVilleartisan($form->get('villeartisan')->getData())
                  ->setIdcorpsmetier($form->get('idcorpsmetier')->getData());

        $em->persist($unArtisan);
        $em->flush();

        return $this->redirectToRoute('bi_modifierArtisan');
      }

      return $this->render('@Bi/Artisan/infoArtisan.html.twig', array('form'=> $form->createView()));
    }
    public function consulterMissionsAction()
    {
      $em = $this->getDoctrine()->getManager();
      $session = new Session();
      $id = $session->get('id');
      $lesMissions = $em->getRepository('BiBundle:Mission')->findByidartisan($id);
      //dump($lesConges);

      return $this->render('@Bi/Artisan/voirMissions.html.twig', array('lesMissions'=>$lesMissions));
    }
}
