<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BiBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Description of EntrepreneurController
 *
 * @author kevintsi
 */
class EntrepreneurController extends Controller {

        public function accueilAction(){

          return $this->render("@Bi/Entrepreneur/accueilEntrepreneur.html.twig",['profil'=>'Entrepreneur']);
        }

        public function action1Action(){

          return $this->render("@Bi/Entrepreneur/action1.html.twig");
       }
       public function action2Action(){

         return $this->render("@Bi/Entrepreneur/action2.html.twig");
      }
}
