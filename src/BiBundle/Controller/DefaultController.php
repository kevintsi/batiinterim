<?php
namespace BiBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    public function indexAction($profil)
    {
            switch($profil){
                case 'artisan':
                    $response = $this->renderView('@Bi/Artisan/accueilArtisan.html.twig',['profil'=>'Artisan']);
                    return new Response($response);
                break;
                case 'entrepreneur':
                    $response = $this->renderView('@Bi/Entrepreneur/accueilEntrepreneur.html.twig',['profil'=>'Entrepreneur']);
                    return new Response($response);
                break;
                case 'gestionnaire':
                    $response = $this->renderView('@Bi/Gestionnaire/accueilGestionnaire.html.twig',['profil'=>'Gestionnaire']);
                    return new Response($response);
                break;
            }
    }
    public function accueilAction(){
        return $this->render("@Bi/Default/index.html.twig");
    }
}
