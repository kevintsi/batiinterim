<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BiBundle\Controller;

use BiBundle\Entity\Entrepreneur;
use BiBundle\Form\EntrepreneurType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BiBundle\Entity\Artisan;
use BiBundle\Form\ArtisanType;
use Symfony\Component\HttpFoundation\Request;


/**
 * Description of GestionnaireController
 *
 * @author kevintsi
 */
class GestionnaireController extends Controller {

      public function accueilAction()
    {
        return $this->render('@Bi/Gestionnaire/accueilGestionnaire.html.twig',['profil'=>'Gestionnaire']);
    }
     public function gestionFicheArtisanAction()
    {
        return $this->render('@Bi/Gestionnaire/gestionFicheArtisan.html.twig');
    }
     public function gestionFicheEntrepreneurAction()
    {
        return $this->render('@Bi/Gestionnaire/gestionFicheEntrepreneur.html.twig');
    }
     public function gestionCouranteAction()
    {
        return $this->render('@Bi/Gestionnaire/gestionCourante.html.twig');
    }

    /* GESTION ARTISAN */

    public function createFormArtisanAction(Request $request){
      $artisan = new Artisan();
      $form = $this->createForm(ArtisanType::class, $artisan);

      $form->handleRequest($request);

      if($form->isSubmitted())
      {
        //dump($form->get('idcorpsmetier')->getData());
        $em = $this->getDoctrine()->getManager();

        $nom = $form->get('nomartisan')->getData();
        $prenom = $form->get('prenomartisan')->getData();
        $login = substr($nom, 0, 1).substr($prenom, 0, 9);
        dump(strtolower($login));

        $artisan->setNomartisan($form->get('nomartisan')->getData())
                ->setPrenomartisan($form->get('prenomartisan')->getData())
                ->setDatenaissanceartisan($form->get('datenaissanceartisan')->getData())
                ->setVillenaissanceartisan($form->get('villenaissanceartisan')->getData())
                ->setPaysnaissanceartisan($form->get('paysnaissanceartisan')->getData())
                ->setTelephoneartisan($form->get('telephoneartisan')->getData())
                ->setAdresseartisan($form->get('adresseartisan')->getData())
                ->setCpartisan($form->get('cpartisan')->getData())
                ->setVilleartisan($form->get('villeartisan')->getData())
                ->setIdcorpsmetier($form->get('idcorpsmetier')->getData())
                ->setLoginartisan(strtolower($login))
                ->setPremiereconnexion(1);
        dump($artisan);
        $em->persist($artisan);
        $em->flush();
        return $this->render('@Bi/Gestionnaire/valideCreationArtisan.html.twig');
      }

      return $this->render('@Bi/Gestionnaire/formcreerArtisan.html.twig', array('form'=>$form->createView()));
    }
    public function supprimerFicheArtisanAction(Request $request)
    {
        $form = $this->createFormBuilder()
                     ->add('artisan', EntityType::class,
                            array('label'=>'Sélectionner un artisan : ',
                                    'class'=>'BiBundle:Artisan',
                                    'choice_label'=> 'nomArtisan',
                                    'multiple'=>false))
                     ->add('Supprimer', SubmitType::class, array('attr'=>array('class'=>'btn btn-primary')))
                     ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // Récupérer l’id correspondant à l’item dans la liste déroulante
            $id = $form->get('artisan')->getData()->getId();
            $em = $this->getDoctrine()->getManager();
            // Récupérer l’objet d’entité correspondant
            $Artisan = $this->getDoctrine()->getRepository('BiBundle:Artisan')->find($id);
            // Supprimer l’objet d’entité
            $em->remove($Artisan);
            $em->flush();
            // Votre code
            return $this->render("@Bi/Gestionnaire/suppressionArtisanValide.html.twig", array('data'=>$Artisan));
        }

        return $this->render("@Bi/Gestionnaire/supprimerArtisan.html.twig", array('form'=>$form->createView()));
    }
    public function consulterFicheArtisansAction()
    {
      $em = $this->getDoctrine()->getManager();
      $lesArtisans = $em->getRepository('BiBundle:Artisan')->findAll();
      //dump($lesArtisans);

      return $this->render('@Bi/Gestionnaire/voirArtisans.html.twig', array('lesArtisans'=>$lesArtisans));
    }
    public function detailArtisanAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $detailArtisan = $em->getRepository('BiBundle:Artisan')->find($id);
      dump($detailArtisan);

      return $this->render('@Bi/Gestionnaire/detailArtisan.html.twig', array('detailArtisan'=>$detailArtisan));
    }

    /*
      GESTION ENTREPRISE
    */
    public function createFormEntrepreneurAction(Request $request)
    {
      $entrepreneur = new Entrepreneur();
      $form = $this->createForm(EntrepreneurType::class, $entrepreneur);

      $form->handleRequest($request);

      if($form->isSubmitted())
      {
        //dump($form->get('idcorpsmetier')->getData());
        $em = $this->getDoctrine()->getManager();
        $entrepreneur = new Entrepreneur();

        $nom = $form->get('nomresponsable')->getData();
        $prenom = $form->get('prenomresponsable')->getData();
        $login = substr($nom, 0, 1).substr($prenom, 0, 9);
        dump(strtolower($login));

        $entrepreneur->setNomresponsable($form->get('nomresponsable')->getData())
                ->setPrenomresponsable($form->get('prenomresponsable')->getData())
                ->setRaisonsociale($form->get('raisonsociale')->getData())
                ->setTelresponsable($form->get('telresponsable')->getData())
                ->setTelephoneentreprise($form->get('telephoneentreprise')->getData())
                ->setAdresseentreprise($form->get('adresseentreprise')->getData())
                ->setCpentreprise($form->get('cpentreprise')->getData())
                ->setVilleentreprise($form->get('villeentreprise')->getData())
                ->setLoginentreprise(strtolower($login))
                ->setPremiereconnexion(1);
        dump($entrepreneur);
        $em->persist($entrepreneur);
        $em->flush();
        return $this->render('@Bi/Gestionnaire/valideCreationEntreprise.html.twig');
      }

      return $this->render('@Bi/Gestionnaire/formcreerEntreprise.html.twig', array('form'=>$form->createView()));
    }
    public function consulterFicheEntrepreneursAction()
    {
      $em = $this->getDoctrine()->getManager();
      $lesEntrepreneurs = $em->getRepository('BiBundle:Entrepreneur')->findAll();
      //dump($lesArtisans);

      return $this->render('@Bi/Gestionnaire/voirEntrepreneurs.html.twig', array('lesEntrepreneurs'=>$lesEntrepreneurs));
    }
    public function supprimerFicheEntrepreneurAction(Request $request)
    {
        $form = $this->createFormBuilder()
                     ->add('entrepreneur', EntityType::class,
                            array('label'=>'Sélectionner un entrepreneur : ',
                                    'class'=>'BiBundle:Entrepreneur',
                                    'choice_label'=> 'nomResponsable',
                                    'multiple'=>false))
                     ->add('Supprimer', SubmitType::class, array('attr'=>array('class'=>'btn btn-primary')))
                     ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // Récupérer l’id correspondant à l’item dans la liste déroulante
            $id = $form->get('entrepreneur')->getData()->getId();
            $em = $this->getDoctrine()->getManager();
            // Récupérer l’objet d’entité correspondant
            $Entrepreneur = $this->getDoctrine()->getRepository(Entrepreneur::class)->find($id);
            // Supprimer l’objet d’entité
            $em->remove($Entrepreneur);
            $em->flush();
            // Votre code
            return $this->render("@Bi/Gestionnaire/suppressionEntrepreneurValide.html.twig", array('data'=>$Entrepreneur));
        }

        return $this->render("@Bi/Gestionnaire/supprimerEntrepreneur.html.twig", array('form'=>$form->createView()));
    }
    public function detailEntrepreneurAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $detailEntrepreneur = $em->getRepository('BiBundle:Entrepreneur')->find($id);
      dump($detailEntrepreneur);

      return $this->render('@Bi/Gestionnaire/detailEntrepreneur.html.twig', array('detailEntrepreneur'=>$detailEntrepreneur));
    }

    /*
        AUTRES
    */

    /*public function initCorpsMetierAction(){
        $tabCorpsMetier = array("charpentier bois","constructeur bois","couvreur","étancheur",
                                "miroitier","chauffagiste","électricien");

        $doctrine = $this->getDoctrine();

        for($i = 0; $i<count($tabCorpsMetier); $i++){
            $corpsMetier = new CorpsMetier();
            $corpsMetier->setLibelleCorpsMetier($tabCorpsMetier[$i]);
            $em = $doctrine->getManager();
            $em->persist($corpsMetier);
        }

        $em->flush();

        return $this->render("@Bi/Gestionnaire/gestionCourante.html.twig");
    }
    public function addCorpsMetierAction($metier){
       $doctrine = $this->getDoctrine();
       $corpsMetier = new CorpsMetier();
       $corpsMetier->setLibelleCorpsMetier($metier);
       $em = $doctrine->getManager();
       $em->persist($corpsMetier);
       $em->flush();

       return $this->render("@Bi/Gestionnaire/ajoutMetierConfirme.html.twig", array('data'=>$corpsMetier));
    }
    public function createFormCorpsMetierAction(Request $request){
        $corpsMetier = new CorpsMetier();
        $form = $this->createForm(CorpsMetierType::class, $corpsMetier);

        $form->handleRequest($request);

        if (($form->isSubmitted() && $form->isValid())){
            $donnees = $form->get('libelleCorpsMetier')->getData();
            return $this->redirectToRoute("tk_bat_addCorpsMetier",array('metier'=>$donnees));
        }

        return $this->render("@Bi/Gestionnaire/ajoutMetier.html.twig",array('form' => $form->createView()));
    }
    public function initSecteurAction(){
        $tabSecteur = array("Structure et gros oeuvre","enveloppe extérieure","équipement technique");

        $doctrine = $this->getDoctrine();

        for($i = 0; $i<count($tabSecteur); $i++){
            $secteur = new Secteur();
            $secteur->setLibelleSecteur($tabSecteur[$i]);
            $em = $doctrine->getManager();
            $em->persist($secteur);
        }

        $em->flush();

        return $this->render("@Bi/Gestionnaire/gestionCourante.html.twig");
    }
        public function addSecteurAction($secteur){
       $doctrine = $this->getDoctrine();
       $leSecteur = new Secteur();
       $leSecteur->setLibelleSecteur($secteur);
       $em = $doctrine->getManager();
       $em->persist($leSecteur);
       $em->flush();

       return $this->render("@Bi/Gestionnaire/ajoutSecteurConfirme.html.twig", array('data'=>$leSecteur));
    }
    public function createFormSecteurAction(Request $request){
        $secteur = new Secteur();
        $form = $this->createForm(SecteurType::class, $secteur);

        $form->handleRequest($request);

        if (($form->isSubmitted() && $form->isValid())){
            $donnees = $form->get('libelleSecteur')->getData();
            return $this->redirectToRoute("tk_bat_addSecteur",array('secteur'=>$donnees));
        }

        return $this->render("@Bi/Gestionnaire/ajoutSecteur.html.twig",array('form' => $form->createView()));
    }
    public function removeCorpsMetierAction(Request $request) {
        $form = $this->createFormBuilder()
                     ->add('corpsMetier', EntityType::class,
                            array('label'=>'Corps Métiers ',
                                    'class'=>'TKBatBundle:CorpsMetier',
                                    'choice_label'=> 'libelleCorpsMetier',
                                    'multiple'=>false))
                     ->add('Supprimer', SubmitType::class)
                     ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // Récupérer l’id correspondant à l’item dans la liste déroulante
            $id = $form->get('corpsMetier')->getData()->getId();
            $em = $this->getDoctrine()->getManager();
            // Récupérer l’objet d’entité correspondant
            $leCorpsMetier = $this->getDoctrine()->getRepository(CorpsMetier::class)->find($id);
            // Supprimer l’objet d’entité
            $em->remove($leCorpsMetier);
            $em->flush();
            // Votre code
            return $this->render("@Bi/Gestionnaire/suppressionCorpsMetierValide.html.twig", array('data'=>$leCorpsMetier));
        }

        return $this->render("@Bi/Gestionnaire/supprimerCorpsMetier.html.twig", array('form'=>$form->createView()));
    }*/
}
