<?php

namespace BiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Secteur
 *
 * @ORM\Table(name="Secteur")
 * @ORM\Entity(repositoryClass="BiBundle\Repository\SecteurRepository")
 */
class Secteur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleSecteur", type="string", length=128, nullable=true)
     */
    private $libellesecteur;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libellesecteur
     *
     * @param string $libellesecteur
     *
     * @return Secteur
     */
    public function setLibellesecteur($libellesecteur)
    {
        $this->libellesecteur = $libellesecteur;

        return $this;
    }

    /**
     * Get libellesecteur
     *
     * @return string
     */
    public function getLibellesecteur()
    {
        return $this->libellesecteur;
    }
}
