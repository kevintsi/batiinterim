<?php

namespace BiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Affecter
 *
 * @ORM\Table(name="Affecter", indexes={@ORM\Index(name="FK_Affecter_Artisan", columns={"idArtisan"}), @ORM\Index(name="FK_Affecter_Mission", columns={"idMission"})})
 * @ORM\Entity(repositoryClass="BiBundle\Repository\AffecterRepository")
 */
class Affecter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAffectation", type="date", nullable=true)
     */
    private $dateaffectation;

    /**
     * @var string
     *
     * @ORM\Column(name="etatAffectation", type="string", length=1, nullable=true)
     */
    private $etataffectation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateValidation", type="date", nullable=true)
     */
    private $datevalidation;

    /**
     * @var \Artisan
     *
     * @ORM\ManyToOne(targetEntity="Artisan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idArtisan", referencedColumnName="id")
     * })
     */
    private $idartisan;

    /**
     * @var \Mission
     *
     * @ORM\ManyToOne(targetEntity="Mission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMission", referencedColumnName="id")
     * })
     */
    private $idmission;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateaffectation
     *
     * @param \DateTime $dateaffectation
     *
     * @return Affecter
     */
    public function setDateaffectation($dateaffectation)
    {
        $this->dateaffectation = $dateaffectation;

        return $this;
    }

    /**
     * Get dateaffectation
     *
     * @return \DateTime
     */
    public function getDateaffectation()
    {
        return $this->dateaffectation;
    }

    /**
     * Set etataffectation
     *
     * @param string $etataffectation
     *
     * @return Affecter
     */
    public function setEtataffectation($etataffectation)
    {
        $this->etataffectation = $etataffectation;

        return $this;
    }

    /**
     * Get etataffectation
     *
     * @return string
     */
    public function getEtataffectation()
    {
        return $this->etataffectation;
    }

    /**
     * Set datevalidation
     *
     * @param \DateTime $datevalidation
     *
     * @return Affecter
     */
    public function setDatevalidation($datevalidation)
    {
        $this->datevalidation = $datevalidation;

        return $this;
    }

    /**
     * Get datevalidation
     *
     * @return \DateTime
     */
    public function getDatevalidation()
    {
        return $this->datevalidation;
    }

    /**
     * Set idartisan
     *
     * @param \BiBundle\Entity\Artisan $idartisan
     *
     * @return Affecter
     */
    public function setIdartisan(\BiBundle\Entity\Artisan $idartisan = null)
    {
        $this->idartisan = $idartisan;

        return $this;
    }

    /**
     * Get idartisan
     *
     * @return \BiBundle\Entity\Artisan
     */
    public function getIdartisan()
    {
        return $this->idartisan;
    }

    /**
     * Set idmission
     *
     * @param \BiBundle\Entity\Mission $idmission
     *
     * @return Affecter
     */
    public function setIdmission(\BiBundle\Entity\Mission $idmission = null)
    {
        $this->idmission = $idmission;

        return $this;
    }

    /**
     * Get idmission
     *
     * @return \BiBundle\Entity\Mission
     */
    public function getIdmission()
    {
        return $this->idmission;
    }
}
