<?php

namespace BiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Travailler
 *
 * @ORM\Table(name="Travailler", indexes={@ORM\Index(name="FK_Travailler_Secteur", columns={"idSecteur"}), @ORM\Index(name="FK_Travailler_Entrepreneur", columns={"idEntrepreneur"})})
 * @ORM\Entity(repositoryClass="BiBundle\Repository\TravaillerRepository")
 */
class Travailler
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Entrepreneur
     *
     * @ORM\ManyToOne(targetEntity="Entrepreneur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEntrepreneur", referencedColumnName="id")
     * })
     */
    private $identrepreneur;

    /**
     * @var \Secteur
     *
     * @ORM\ManyToOne(targetEntity="Secteur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSecteur", referencedColumnName="id")
     * })
     */
    private $idsecteur;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identrepreneur
     *
     * @param \BiBundle\Entity\Entrepreneur $identrepreneur
     *
     * @return Travailler
     */
    public function setIdentrepreneur(\BiBundle\Entity\Entrepreneur $identrepreneur = null)
    {
        $this->identrepreneur = $identrepreneur;

        return $this;
    }

    /**
     * Get identrepreneur
     *
     * @return \BiBundle\Entity\Entrepreneur
     */
    public function getIdentrepreneur()
    {
        return $this->identrepreneur;
    }

    /**
     * Set idsecteur
     *
     * @param \BiBundle\Entity\Secteur $idsecteur
     *
     * @return Travailler
     */
    public function setIdsecteur(\BiBundle\Entity\Secteur $idsecteur = null)
    {
        $this->idsecteur = $idsecteur;

        return $this;
    }

    /**
     * Get idsecteur
     *
     * @return \BiBundle\Entity\Secteur
     */
    public function getIdsecteur()
    {
        return $this->idsecteur;
    }
}
